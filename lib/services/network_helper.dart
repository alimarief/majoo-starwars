import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:starwars/models/people.dart';

class NetworkHelper {
  Future<People> fetchPeople() async {
    var url = 'https://swapi.dev/api/people/';

    final response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return People.fromJson(jsonResponse);
    } else {
      print('Request failed with status: ${response.statusCode}.');
      throw Exception('Failed to load People');
    }
  }
}
