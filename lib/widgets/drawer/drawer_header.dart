import 'package:flutter/material.dart';
import 'package:starwars/pages/login_page.dart';

class NavDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      child: Row(
        children: <Widget>[
          Container(
            width: 50.0,
            height: 50.0,
            child: CircleAvatar(
              backgroundImage: NetworkImage(photoUrl),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Container(
            alignment: Alignment.centerLeft,
            width: 200.0,
            height: 55.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  name,
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Color(0xFF5666A6),
      ),
    );
  }
}
