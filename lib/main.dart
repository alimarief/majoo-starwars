import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:starwars/pages/favorite_page.dart';
import 'package:starwars/pages/home_page.dart';
import 'package:starwars/pages/login_page.dart';
import 'package:starwars/pages/profile_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(StarWarsApp());
}

class StarWarsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/home': (context) => HomePage(),
        '/favorite': (context) => FavoritePage(),
        '/profile': (context) => ProfilePage(),
      },

    );
  }
}
