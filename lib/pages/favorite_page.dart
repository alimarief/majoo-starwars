import 'package:flutter/material.dart';
import 'package:starwars/widgets/drawer/nav_drawer.dart';

class FavoritePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('Favorite'),
      ),
      body: Center(
        child: Text('Favorite Screen'),
      ),
    );
  }
}
