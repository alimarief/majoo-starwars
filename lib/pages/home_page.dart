import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:starwars/models/people.dart';
import 'package:starwars/services/auth_google_service.dart';
import 'package:starwars/services/network_helper.dart';
import 'package:starwars/widgets/drawer/nav_drawer.dart';

import 'login_page.dart';

AuthServices authServices = AuthServices();
NetworkHelper networkHelper = NetworkHelper();
Future<People> futurePeople;
People peoples = People();
bool iconStar = false;
var colour = Colors.grey;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    futurePeople = networkHelper.fetchPeople();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Beranda'),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.person)),
              Tab(icon: Icon(Icons.child_care)),
            ],
          ),
        ),
        drawer: NavDrawer(),
        body: TabBarView(
          children: [
            Container(
              child: Center(
                child: FutureBuilder(
                  future: futurePeople,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      peoples = snapshot.data;
                      return ListView.builder(
                        itemCount: peoples.results.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: <Widget>[
                              ListTile(
                                title: Text(peoples.results[index].name),
                                trailing: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      colour = Colors.yellow;
                                    });
                                  },
                                  icon: Icon(
                                    Icons.star,
                                    color: colour,
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
            ),
            Container(
              child: Center(
                child: FutureBuilder(
                  future: futurePeople,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      peoples = snapshot.data;
                      return GridView.count(
                        crossAxisCount: 2,
                        children: List.generate(10, (index) {
                          return Center(
                            child: Text(
                              peoples.results[index].name,
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          );
                        }),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
