import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

void main() {
  NetworkHelper().peopleService();
}

class NetworkHelper {
  Future<String> peopleService() async {
    var url = 'https://swapi.dev/api/people/';

    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      // var data = jsonResponse['result'][0]['name'];
      print('DATA:  $jsonResponse.');
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }
}
